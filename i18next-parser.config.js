module.exports = {
  createOldCatalogs: true,
  indentation: 2,
  input: ['src/**/*.{js,jsx,ts,tsx}'],
  lexers: {
    js: ['JsxLexer'],
    ts: ['JsxLexer'],
    jsx: ['JsxLexer'],
    tsx: ['JsxLexer'],

    default: ['JsxLexer'],
  },
  locales: ['en'],
  output: 'src/i18n/locales/$LOCALE/$NAMESPACE.json',
  sort: true,
  verbose: true,
};
