const prettierOptions = require('./prettier.config');

module.exports = {
  extends: 'universe/native',
  plugins: ['import', 'prettier'],
  rules: {
    'prettier/prettier': ['error', prettierOptions],
    // '@typescript-eslint/no-unused-vars': ['error', { vars: 'local' }],
    '@typescript-eslint/no-unused-vars': ['off'],
    'import/no-unresolved': 'error',
    'no-undef': 'off',
    'no-unused-vars': 'off',
  },
  settings: {
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
      },
    },
  },
};
