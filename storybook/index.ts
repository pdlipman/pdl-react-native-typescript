import AsyncStorage from '@react-native-community/async-storage';
import { withKnobs } from '@storybook/addon-knobs';
import {
  getStorybookUI,
  configure,
  addDecorator,
} from '@storybook/react-native';

// @ts-ignore
import { loadStories } from './storyLoader';

// enables knobs for all stories
addDecorator(withKnobs);

// import stories
// const loadStories = () => {
//   const req = require.context('../src', true, /\.story\.tsx?$/);
//   req.keys().forEach((story) => req(story));
// };

configure(() => {
  loadStories();
}, module);

// Refer to https://github.com/storybookjs/storybook/tree/master/app/react-native#start-command-parameters
// To find allowed options for getStorybookUI
const StorybookUIRoot = getStorybookUI({ asyncStorage: AsyncStorage });

export default StorybookUIRoot;
