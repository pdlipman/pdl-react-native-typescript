// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
  require('../src/components/button/stories/index.stories');
  require('../src/components/message/stories/index.stories');
  require('../src/styles/colors/stories/index.stories');
}

const stories = [
  '../src/components/button/stories/index.stories',
  '../src/components/message/stories/index.stories',
  '../src/styles/colors/stories/index.stories',
];

module.exports = {
  loadStories,
  stories,
};
