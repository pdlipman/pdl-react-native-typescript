module.exports = function (api) {
  api.cache(true);
  return {
    plugins: [
      [
        'module-resolver',
        {
          alias: {
            '@components': './src/components',
            '@containers': './src/containers',
            '@hooks': './src/hooks',
            '@i18n': './src/i18n/index',
            '@providers': './src/providers',
            '@state': './src/store/state',
            '@store': './src/store/index',
            '@styled-components': './src/styled-components',
            '@styles': './src/styles',
            '@utils': './src/utils',
          },
        },
      ],
    ],
    presets: ['babel-preset-expo'],
  };
};
