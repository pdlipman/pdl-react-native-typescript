import { HomeState } from '@containers/home/redux/types';
import { MainState } from '@containers/main/redux/types';
import { AuthState } from '@providers/firebase/auth/types';
import { LoginState } from '@providers/firebase/login/types';
import { PostsState } from '@providers/firebase/posts/types';
import { RegisterState } from '@providers/firebase/register/types';

export interface ApplicationState {
  readonly auth: AuthState;
  readonly home: HomeState;
  readonly login: LoginState;
  readonly main: MainState;
  readonly posts: PostsState;
  readonly register: RegisterState;
}
