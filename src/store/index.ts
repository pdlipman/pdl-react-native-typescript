import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import logger from 'redux-logger';
import {
  persistStore,
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
} from 'redux-persist';
import createSagaMiddleware from 'redux-saga';

import { rootReducer } from './rootReducer';
import { rootSaga } from './rootSaga';

const developmentMode = process.env.NODE_ENV === 'development';
const reduxPersistActions = [FLUSH, PAUSE, PERSIST, PURGE, REGISTER, REHYDRATE];
const sagaMiddleware = createSagaMiddleware();
const middleware = [
  ...getDefaultMiddleware({
    serializableCheck: { ignoredActions: [...reduxPersistActions] },
    thunk: false,
  }),
  sagaMiddleware,
];

if (developmentMode) {
  middleware.push(logger);
}

const store = configureStore({
  reducer: rootReducer,
  devTools: developmentMode,
  middleware,
});

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);
export default store;
