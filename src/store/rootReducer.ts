import { combineReducers } from '@reduxjs/toolkit';

import { homeReducer } from '@containers/home/redux/reducer';
import { mainReducer } from '@containers/main/redux/reducer';
import authReducer from '@providers/firebase/auth/reducer';
import loginReducer from '@providers/firebase/login/reducer';
import postsReducer from '@providers/firebase/posts/reducer';
import registerReducer from '@providers/firebase/register/reducer';

export const rootReducer = combineReducers({
  auth: authReducer,
  home: homeReducer,
  login: loginReducer,
  main: mainReducer,
  posts: postsReducer,
  register: registerReducer,
});
