import { all, call, spawn } from 'redux-saga/effects';

import authSaga from '@providers/firebase/auth/saga';
import loginSaga from '@providers/firebase/login/saga';
import postsSaga from '@providers/firebase/posts/saga';
import registerSaga from '@providers/firebase/register/saga';

function* loaded() {
  yield call(console.log, 'Sagas loaded');
}

export function* rootSaga() {
  const sagas = [authSaga, loaded, loginSaga, postsSaga, registerSaga];

  yield all(
    sagas.map((saga) =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e) {
            console.log(e);
          }
        }
      })
    )
  );
}
