import { StatusBarStyle } from 'react-native';
import { DefaultTheme } from 'styled-components/native';

interface Color {
  background: string;
  border: string;
  font: string;
  icon: string;
}

declare module 'styled-components' {
  export interface DefaultTheme {
    borderRadius: string;
    borderWidth: string;
    fontSizes: { small: string; medium: string; large: string };
    color: {
      background: string;
      font: string;
      shadowColor: string;
      statusBarContent: StatusBarStyle;
      error: Color;
      primary: Color;
      secondary: Color;
    };
  }
}

const shared = {
  borderRadius: '100px',
  borderWidth: '1px',
  fontSizes: {
    small: '16px',
    medium: '21px',
    large: '34px',
  },
};

const error = {
  background: '#ffe8e6',
  border: '#db2828',
  font: '#912d2b',
  icon: '#db2828',
};

const primary = {
  background: '#6184d8',
  border: '#533a71',
  font: 'white',
  icon: 'white',
};

const secondary = {
  background: '#607d8b',
  border: '#263238',
  font: 'white',
  icon: 'white',
};

const standard: DefaultTheme = {
  ...shared,
  color: {
    background: 'white',
    font: '#0b090a',
    shadowColor: '#000',
    statusBarContent: 'default',
    error,
    primary,
    secondary,
  },
};

const dark: DefaultTheme = {
  ...standard,
  color: {
    background: '#0b090a',
    font: '#e8eddf',
    shadowColor: '#f0f465',
    statusBarContent: 'light-content',
    error,
    primary,
    secondary,
  },
};

type ThemeStyle = 'standard' | 'dark';

const themes = (theme: ThemeStyle): DefaultTheme => {
  switch (theme) {
    case 'dark': {
      return dark;
    }
    default: {
      return standard;
    }
  }
};

export { themes, ThemeStyle };
