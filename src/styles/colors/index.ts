// Background colors
const bgPrimary = '#fff';
const bgSecondary = '#f4fbfe';
const bgOverlay = '#000';
const bgMap = '#f1f4f6';

const primary = {
  background: '#ff5722',
  border: '#bf360c',
  font: 'white',
  icon: 'white',
};

const secondary = {
  background: '#607d8b',
  border: '#263238',
  font: 'white',
  icon: 'white',
};

const standard = {
  background: 'white',
  primary,
  secondary,
};

const dark = {
  background: 'black',
  primary,
  secondary,
};
export type ThemeStyle = 'standard' | 'dark';

const themes = (theme: ThemeStyle) => {
  switch (theme) {
    case 'dark': {
      return dark;
    }
    default: {
      return standard;
    }
  }
};

export { themes };

export default {
  bgPrimary,
  bgSecondary,
  bgOverlay,
  bgMap,
};
