import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Text, View } from 'react-native';

import { CenterView } from '@utils/storybook';

import colors from '../';

const Palette = ({ name, value }) => {
  return (
    <View
      style={{
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 10,
        margin: 10,
        padding: 10,
      }}
    >
      <View
        style={{
          width: 100,
          height: 100,
          backgroundColor: value,
          borderColor: 'black',
          borderWidth: 1,
          borderRadius: 10,
        }}
      />
      <Text style={{ paddingTop: 10 }}>{name}</Text>
      <Text>{value}</Text>
    </View>
  );
};

storiesOf('Color Palette', module)
  .addDecorator((getStory: any) => <CenterView>{getStory()}</CenterView>)
  .add('Default', () => {
    const colorPalette = Object.keys(colors).map((name) => {
      return <Palette key={name} name={name} value={colors[name]} />;
    });
    return <>{colorPalette}</>;
  });
