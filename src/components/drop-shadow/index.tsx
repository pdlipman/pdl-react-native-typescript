import React, { ReactNode, useRef } from 'react';
import { Animated } from 'react-native';
import styled from 'styled-components/native';

import { useTheme } from '@hooks';

const StyledView = styled(Animated.View)`
  border-radius: ${({ theme }) => theme.borderRadius};
`;

interface Props {
  children: ReactNode;
}

const DropShadow = ({ children }: Props) => {
  const fadeValue = useRef(new Animated.Value(1)).current;
  const theme = useTheme();

  const elevationInterpolation = fadeValue.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 12],
  });

  const opacityInterpolation = fadeValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0.18, 0.37],
  });

  const shadowOffsetHeightInterpolation = fadeValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 2],
  });

  const shadowRadiusInterpolation = fadeValue.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 7.49],
  });

  const handlePressIn = () => {
    Animated.timing(fadeValue, {
      toValue: 0,
      useNativeDriver: true,
      duration: 200,
    }).start();
  };

  const handlePressOut = () => {
    Animated.timing(fadeValue, {
      toValue: 1,
      useNativeDriver: true,
      duration: 50,
    }).start();
  };

  return (
    <StyledView
      onTouchEnd={handlePressOut}
      onTouchStart={handlePressIn}
      style={{
        elevation: elevationInterpolation,
        shadowColor: theme.color.shadowColor,
        shadowOffset: {
          width: 0,
          height: shadowOffsetHeightInterpolation,
        },
        shadowOpacity: opacityInterpolation,
        shadowRadius: shadowRadiusInterpolation,
      }}
      testID="drop-shadow-component"
    >
      {children}
    </StyledView>
  );
};

export default DropShadow;
