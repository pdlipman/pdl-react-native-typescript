import React from 'react';

import DropShadow from '@components/drop-shadow';
import { renderWithThemeProvider } from '@utils/test';

describe('<DropShadow />', () => {
  it('renders correctly', () => {
    const view = renderWithThemeProvider({
      ui: (
        <DropShadow>
          <></>
        </DropShadow>
      ),
    });
    const { getByTestId } = view;
    expect(getByTestId('drop-shadow-component')).toBeTruthy();
  });
});
