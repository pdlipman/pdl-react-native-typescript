import React, { ReactNode, useState } from 'react';
import {
  NativeSyntheticEvent,
  Platform,
  TextInputFocusEventData,
  TextInputProps,
} from 'react-native';
import styled, { css } from 'styled-components/native';

const StyledContainer = styled.View`
  margin-bottom: 10px;
  margin-top: 10px;
  width: 100%;
`;

const StyledError = styled.Text`
  color: red;
  font-size: ${({ theme }) => theme.fontSizes.small};
`;

const StyledInput = styled.TextInput`
  flex: 1;
  font-size: ${({ theme }) => theme.fontSizes.medium};
  padding-bottom: 10px;
  padding-left: 10px;
  padding-top: 10px;
  ${Platform.select({
    web: css`
      outline-width: 0px;
    `,
  })};
`;

const StyledInputWrapper = styled.View<{
  error?: boolean;
  isFocused?: boolean;
}>`
  align-items: center;
  border-color: ${({ error }) => (error ? 'red' : 'black')};
  border-radius: ${({ theme }) => theme.borderRadius};
  border-width: ${({ isFocused }) => (isFocused ? '4px' : '2px')};
  flex-direction: row;
  justify-content: center;
  padding: ${({ isFocused }) => (isFocused ? '0px' : '2px')};
`;

const StyledLabel = styled.Text<{ error?: boolean }>`
  color: ${({ error }) => (error ? 'red' : 'black')};
  font-size: ${({ theme }) => theme.fontSizes.medium};
`;

interface Props extends TextInputProps {
  endAdornment?: ReactNode;
  error?: boolean;
  helperText?: string;
  label?: string;
  value: string;
}

const Default = ({
  autoCapitalize,
  autoCompleteType,
  endAdornment,
  error,
  helperText,
  keyboardType,
  label,
  onBlur,
  onChangeText,
  secureTextEntry,
  textContentType,
  value,
}: Props) => {
  const [isFocused, setIsFocused] = useState(false);

  const handleBlur = (event: NativeSyntheticEvent<TextInputFocusEventData>) => {
    setIsFocused(false);
    onBlur?.(event);
  };

  return (
    <StyledContainer>
      <StyledLabel error={error}>{label}</StyledLabel>
      <StyledInputWrapper error={error} isFocused={isFocused}>
        <StyledInput
          accessibilityLabel={label}
          autoCapitalize={autoCapitalize}
          autoCompleteType={autoCompleteType}
          keyboardType={keyboardType}
          onBlur={handleBlur}
          onChangeText={onChangeText}
          onFocus={() => setIsFocused(true)}
          secureTextEntry={secureTextEntry}
          textContentType={textContentType}
          value={value}
        />
        {endAdornment}
      </StyledInputWrapper>
      <StyledError>{error && helperText}&nbsp;</StyledError>
    </StyledContainer>
  );
};

export default Default;
