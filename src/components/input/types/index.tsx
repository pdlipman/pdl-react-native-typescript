import Default from './default';
import Email from './email';
import Password from './password';

export { Default, Email, Password };
