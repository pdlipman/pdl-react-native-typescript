import { Feather } from '@expo/vector-icons';
import React, { useState } from 'react';
import {
  KeyboardTypeOptions,
  NativeSyntheticEvent,
  Platform,
  Pressable,
  TextInputFocusEventData,
  TextInputProps,
} from 'react-native';
import styled, { css } from 'styled-components/native';

import i18n from '@i18n';

import Default from '../default';

const StyledIcon = styled(Feather)<{ error?: boolean }>`
  color: ${({ error }) => (error ? 'red' : 'black')};
  font-size: 24px;
  padding-bottom: 6px;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 6px;
  ${Platform.select({
    web: css`
      cursor: pointer;
    `,
  })}
`;

const StyledPressable = styled(Pressable)`
  ${Platform.select({
    web: css`
      outline-width: 0px;
    `,
  })};
`;

interface Props extends TextInputProps {
  error?: boolean;
  helperText?: string;
  label?: string;
  value: string;
}

const Password = ({
  error,
  helperText,
  label = i18n.t('password', '[Password]'),
  onBlur,
  onChangeText,
  value,
}: Props) => {
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  // const [isFocused, setIsFocused] = useState(false);
  const keyboardType = Platform.select<KeyboardTypeOptions>({
    ios: 'ascii-capable',
    android: 'visible-password',
  });

  return (
    <Default
      autoCompleteType="password"
      endAdornment={
        <StyledPressable
          accessibilityLabel="Show/Hide Password"
          onPress={() => setSecureTextEntry(!secureTextEntry)}
        >
          <StyledIcon
            error={error}
            name={secureTextEntry ? 'eye' : 'eye-off'}
          />
        </StyledPressable>
      }
      error={error}
      helperText={helperText}
      keyboardType={secureTextEntry ? undefined : keyboardType}
      label={label}
      onBlur={onBlur}
      onChangeText={onChangeText}
      secureTextEntry={secureTextEntry}
      textContentType="password"
      value={value}
    />
  );
};

export default Password;
