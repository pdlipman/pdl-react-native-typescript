import { Feather } from '@expo/vector-icons';
import React, { useState } from 'react';
import {
  KeyboardTypeOptions,
  NativeSyntheticEvent,
  Platform,
  Pressable,
  TextInputFocusEventData,
  TextInputProps,
} from 'react-native';
import styled, { css } from 'styled-components/native';

import i18n from '@i18n';

import Default from '../default';

interface Props extends TextInputProps {
  error?: boolean;
  helperText?: string;
  label?: string;
  value: string;
}

const Email = ({
  error,
  helperText,
  label = i18n.t('email', '[Email]'),
  onBlur,
  onChangeText,
  value,
}: Props) => {
  return (
    <Default
      autoCapitalize="none"
      autoCompleteType="email"
      error={error}
      helperText={helperText}
      keyboardType="email-address"
      label={label}
      onBlur={onBlur}
      onChangeText={onChangeText}
      textContentType="emailAddress"
      value={value}
    />
  );
};

export default Email;
