import { fireEvent } from '@testing-library/react-native';
import React from 'react';

import Input from '@components/input';
import { renderWithThemeProvider } from '@utils/test';

const label = 'Test Label';
const wrapper = (props = {}) => {
  const defaultProps = {
    label,
    onBlur: () => {},
    onChangeText: () => {},
    value: '',
  };
  const renderedComponent = renderWithThemeProvider({
    ui: <Input {...defaultProps} {...props} />,
  });
  return renderedComponent;
};

describe('<Input />', () => {
  it('renders correctly', () => {
    const { getByLabelText } = wrapper();
    expect(getByLabelText(label)).toBeTruthy();
  });

  it('blur', async () => {
    const props = { onBlur: jest.fn() };
    const { getByLabelText } = wrapper(props);
    const input = getByLabelText(label);
    fireEvent(input, 'blur');
    expect(props.onBlur).toHaveBeenCalledTimes(1);
  });

  it('change text', () => {
    const changeText = 'Hello, world!';
    const props = { onChangeText: jest.fn() };
    const { getByLabelText } = wrapper(props);
    const input = getByLabelText(label);
    fireEvent.changeText(input, changeText);
    expect(props.onChangeText).toHaveBeenCalledWith(changeText);
  });

  it('no error', () => {
    const helperText = 'error';
    const { queryByText } = wrapper({ error: false, helperText });
    expect(queryByText(/error/i)).toBeNull();
  });

  it('error', () => {
    const helperText = 'error';
    const { queryByText } = wrapper({ error: true, helperText });
    expect(queryByText(/error/i)).toBeTruthy();
  });
});
