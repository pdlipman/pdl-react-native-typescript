import React, { ReactNode, useState } from 'react';
import { TextInputProps } from 'react-native';

import { Default, Email, Password } from '@components/input/types';

interface Props extends TextInputProps {
  endAdornment?: ReactNode;
  error?: boolean;
  helperText?: string;
  label?: string;
  type?: 'default' | 'password';
  value: string;
}

const components = {
  default: Default,
  email: Email,
  password: Password,
};

const Input = ({
  error,
  helperText,
  label,
  onBlur,
  onChangeText,
  type = 'default',
  value,
}: Props) => {
  const Component = components[type];

  return (
    <Component
      error={error}
      helperText={helperText}
      label={label}
      onBlur={onBlur}
      onChangeText={onChangeText}
      value={value}
    />
  );
};

export default Input;
export { Email, Password };
