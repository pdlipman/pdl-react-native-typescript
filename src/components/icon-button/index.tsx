import { Feather } from '@expo/vector-icons';
import React from 'react';
import { Pressable, PressableProps } from 'react-native';
import styled from 'styled-components/native';

import DropShadow from '@components/drop-shadow';
import { useTheme } from '@hooks';

const StyledButton = styled(Pressable)`
  align-items: center;
  background-color: ${({ theme }) => theme.color.primary.background};
  border-color: ${({ theme }) => theme.color.primary.border};
  border-radius: ${({ theme }) => theme.borderRadius}
  border-width: ${({ theme }) => theme.borderWidth};
  justify-content: center;
  padding: 10px;
  width: 80px;
  height: 80px;
`;

const StyledIcon = styled(Feather)`
  color: ${({ theme }) => theme.color.primary.icon};
  font-size: 36px;
`;

interface Props extends PressableProps {
  icon: string;
  label?: string;
}
const IconButton = ({ icon, label, onLayout, onPress, style }: Props) => {
  return (
    <DropShadow>
      <StyledButton
        accessibilityLabel={label}
        onPress={onPress}
        onLayout={onLayout}
        style={style}
      >
        <StyledIcon name={icon} />
      </StyledButton>
    </DropShadow>
  );
};

export default IconButton;
