import React from 'react';

import IconButton from '@components/icon-button';
import { renderWithThemeProvider } from '@utils/test';

describe('<IconButton />', () => {
  it('renders correctly', () => {
    const label = 'Test Icon';
    const { getByLabelText } = renderWithThemeProvider({
      ui: <IconButton icon="activity" label={label} />,
    });
    expect(getByLabelText(label)).toBeTruthy();
  });
});
