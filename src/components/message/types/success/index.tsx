import React from 'react';

import Default from '../default';

interface Props {
  content: string;
  title: string;
}

const Success = ({ content, title }: Props) => {
  const colors = {
    background: '#e5f9e7',
    border: '#1ebc30',
    font: '#1a531b',
    icon: '#1ebc30',
  };

  const icon = 'check-square';
  return (
    <Default colors={colors} content={content} icon={icon} title={title} />
  );
};

export default Success;
