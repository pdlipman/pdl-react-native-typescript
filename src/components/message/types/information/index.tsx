import React from 'react';

import Default from '../default';

interface Props {
  content: string;
  title?: string;
}

const Information = ({ content, title }: Props) => {
  const colors = {
    background: '#dff0ff',
    border: '#2185d0',
    font: '#0e566c',
    icon: '#2185d0',
  };

  const icon = 'info';
  return (
    <Default colors={colors} content={content} icon={icon} title={title} />
  );
};

export default Information;
