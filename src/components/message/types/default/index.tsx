import { Feather } from '@expo/vector-icons';
import React from 'react';
import styled from 'styled-components/native';

const StyledBody = styled.Text<{ fontColor: string }>`
  color: ${({ fontColor }) => fontColor};
  font-size: 16px;
`;

const StyledContainer = styled.View<{
  backgroundColor: string;
  borderColor: string;
}>`
  align-items: center;
  background-color: ${({ backgroundColor }) => backgroundColor};
  border-color: ${({ borderColor }) => borderColor};
  border-width: 2px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-bottom: 10px;
  margin-top: 10px;
  padding: 10px;
  width: 100%;
`;

const StyledContent = styled.View`
  flex: 1;
`;

const StyledTitle = styled.Text<{ fontColor: string }>`
  align-items: center;
  color: ${({ fontColor }) => fontColor};
  font-size: 21px;
  font-weight: bold;
  justify-content: center;
`;

const StyledIcon = styled(Feather)<{ iconColor: string }>`
  color: ${({ iconColor }) => iconColor};
  font-size: 36px;
  padding-right: 10px;
`;

const defaultColors = {
  background: '#f8f8f9',
  border: 'black',
  font: 'black',
  icon: 'black',
};

interface Colors {
  background: string;
  border: string;
  font: string;
  icon: string;
}

interface Props {
  colors?: Colors;
  content: string;
  icon?: string;
  title?: string;
}

const Default = ({
  colors = defaultColors,
  content,
  icon = 'message-square',
  title,
}: Props) => {
  return (
    <StyledContainer
      backgroundColor={colors.background}
      borderColor={colors.border}
    >
      <StyledIcon iconColor={colors.icon} name={icon} />
      <StyledContent>
        {title && <StyledTitle fontColor={colors.font}>{title}</StyledTitle>}
        <StyledBody fontColor={colors.font}>{content}</StyledBody>
      </StyledContent>
    </StyledContainer>
  );
};

export default Default;
