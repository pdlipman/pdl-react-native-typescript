import React from 'react';

import Default from '../default';

interface Props {
  content: string;
  title?: string;
}

const Error = ({ content, title }: Props) => {
  const colors = {
    background: '#ffe8e6',
    border: '#db2828',
    font: '#912d2b',
    icon: '#db2828',
  };
  const icon = 'alert-circle';

  return (
    <Default colors={colors} content={content} icon={icon} title={title} />
  );
};

export default Error;
