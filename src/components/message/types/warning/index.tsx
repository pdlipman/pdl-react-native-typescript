import React from 'react';

import Default from '../default';

interface Props {
  content: string;
  title: string;
}

const Warning = ({ content, title }: Props) => {
  const colors = {
    background: '#ffedde',
    border: '#f2711c',
    font: '#794b02',
    icon: '#f2711c',
  };

  const icon = 'alert-triangle';
  return (
    <Default colors={colors} content={content} icon={icon} title={title} />
  );
};

export default Warning;
