import React from 'react';

import {
  Default,
  Error,
  Information,
  Success,
  Warning,
} from '@components/message/types';

interface Props {
  content: string;
  title?: string;
  type?: 'default' | 'error' | 'information' | 'success' | 'warning';
}

const components = {
  default: Default,
  error: Error,
  information: Information,
  success: Success,
  warning: Warning,
};

const Message = ({ content, title, type = 'default' }: Props) => {
  const Component = components[type];
  return <Component title={title} content={content} />;
};

export default Message;
export { Error, Information, Success, Warning };
