import { render } from '@testing-library/react-native';
import React from 'react';

import Message from '@components/message';

describe('<Message />', () => {
  it('renders correctly', () => {
    const props = {
      message: 'Hello, world!',
    };
    const renderedComponent = render(<Message {...props} />);
    const { getByText } = renderedComponent;
    expect(getByText(new RegExp(props.message))).toBeTruthy();
  });
});
