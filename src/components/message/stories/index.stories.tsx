import { text } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { CenterView } from '@utils/storybook';

import Message, { Error, Information, Success, Warning } from '../';

storiesOf('Message', module)
  .addDecorator((getStory: any) => <CenterView>{getStory()}</CenterView>)
  .add('Default', () => (
    <Message
      content={text('message content', 'Default Content')}
      title={text('message title', 'Default Title')}
    />
  ))
  .add('Error', () => (
    <Error
      content={text('message content', 'Error Content')}
      title={text('message title', 'Error Title')}
    />
  ))
  .add('Information', () => (
    <Information
      content={text('message content', 'Information Content')}
      title={text('message title', 'Information Title')}
    />
  ))
  .add('Success', () => (
    <Success
      content={text('message content', 'Success Content')}
      title={text('message title', 'Success Title')}
    />
  ))
  .add('Warning', () => (
    <Warning
      content={text('message content', 'Warning Content')}
      title={text('message title', 'Warning Title')}
    />
  ));
