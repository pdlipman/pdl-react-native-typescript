import React from 'react';
import { TouchableOpacityProps } from 'react-native';

import { useTheme } from '@hooks';

import Default from '../default';

interface Props extends TouchableOpacityProps {
  label?: string;
  title: string;
}
const Primary = ({ label, onPress, title }: Props) => {
  const theme = useTheme();
  const colors = {
    background: theme.color.primary.background,
    border: theme.color.primary.border,
    font: theme.color.primary.font,
    icon: theme.color.primary.icon,
  };

  return (
    <Default colors={colors} label={label} onPress={onPress} title={title} />
  );
};

export default Primary;
