import React from 'react';
import { TouchableOpacityProps } from 'react-native';

import { useTheme } from '@hooks';

import Default from '../default';

interface Props extends TouchableOpacityProps {
  label?: string;
  title: string;
}
const Secondary = ({ label, onPress, title }: Props) => {
  const theme = useTheme();
  const colors = {
    background: theme.color.secondary.background,
    border: theme.color.secondary.border,
    font: theme.color.secondary.font,
    icon: theme.color.secondary.icon,
  };

  return (
    <Default colors={colors} label={label} onPress={onPress} title={title} />
  );
};

export default Secondary;
