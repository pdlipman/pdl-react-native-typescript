import React, { ReactNode } from 'react';
import { Pressable, PressableProps } from 'react-native';
import styled from 'styled-components/native';

import DropShadow from '@components/drop-shadow';

const StyledContainer = styled.View`
  margin-bottom: 10px;
  margin-top: 10px;
  width: 100%;
`;

const StyledButton = styled(Pressable)<{
  backgroundColor: string;
  borderColor: string;
}>`
  align-items: center;
  background-color: ${({ backgroundColor }) => backgroundColor};
  border-color: ${({ borderColor }) => borderColor};
  border-radius: ${({ theme }) => theme.borderRadius}
  border-width: ${({ theme }) => theme.borderWidth};
  justify-content: center;
  padding: 10px;
`;

const StyledText = styled.Text<{ fontColor: string }>`
  color: ${({ fontColor }) => fontColor};
  font-size: ${({ theme }) => theme.fontSizes.medium};
  font-weight: bold;
`;

const defaultColors = {
  background: '#f5f5f5',
  border: '#424242',
  font: '#424242',
  icon: '#424242',
};

interface Colors {
  background: string;
  border: string;
  font: string;
  icon: string;
}

interface Props extends PressableProps {
  colors?: Colors;
  label?: string;
  title: ReactNode | string;
}
const Button = ({ colors = defaultColors, label, onPress, title }: Props) => {
  return (
    <StyledContainer>
      <DropShadow>
        <StyledButton
          accessibilityLabel={label}
          backgroundColor={colors.background}
          borderColor={colors.border}
          onPress={onPress}
        >
          <StyledText fontColor={colors.font}>{title}</StyledText>
        </StyledButton>
      </DropShadow>
    </StyledContainer>
  );
};

export default Button;
