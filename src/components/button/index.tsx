import React from 'react';
import { TouchableOpacityProps } from 'react-native';

import { Default, Primary, Secondary } from '@components/button/types';

const components = {
  default: Default,
  primary: Primary,
  secondary: Secondary,
};

interface Props extends TouchableOpacityProps {
  label?: string;
  type?: 'default' | 'primary' | 'secondary';
  title: string;
}
const Button = ({ label, onPress, title, type = 'default' }: Props) => {
  const Component = components[type];
  return <Component label={label} onPress={onPress} title={title} />;
};

export default Button;
export { Primary, Secondary };
