import { text } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { CenterView, withAction } from '@utils/storybook';

import Button, { Primary, Secondary } from '../';

storiesOf('Button', module)
  .addDecorator((getStory: any) => <CenterView>{getStory()}</CenterView>)
  .add('Default', () => (
    <Button
      onPress={withAction('button-click')}
      title={text('Button text', 'Default Button')}
      type="default"
    />
  ))
  .add('Primary', () => (
    <Primary
      onPress={withAction('button-click')}
      title={text('Button text', 'Primary Button')}
    />
  ))
  .add('Secondary', () => (
    <Secondary
      onPress={withAction('button-click')}
      title={text('Button text', 'Secondary Button')}
    />
  ));
