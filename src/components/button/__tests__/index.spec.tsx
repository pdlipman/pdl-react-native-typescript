import React from 'react';

import Button from '@components/button';
import { renderWithThemeProvider } from '@utils/test';

describe('<Button />', () => {
  it('renders correctly', () => {
    const title = 'Test Button';
    const view = renderWithThemeProvider({ ui: <Button title={title} /> });
    const { getByText } = view;
    expect(getByText(title)).toBeTruthy();
  });
});
