import React from 'react';

import Modal from '@components/modal';
import { renderWithNavigator } from '@utils/test';

describe('<Modal />', () => {
  it('renders correctly', () => {
    const title = 'Test Modal';
    const view = renderWithNavigator({ ui: <Modal title={title} /> });
    const { getByText } = view;
    view.debug();
    expect(getByText(title)).toBeTruthy();
  });
});
