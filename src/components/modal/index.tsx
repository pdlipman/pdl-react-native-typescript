import { useNavigation } from '@react-navigation/native';
import { BlurView } from 'expo-blur';
import React, { ReactNode } from 'react';
import {
  Modal as NativeModal,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import styled from 'styled-components/native';

import IconButton from '@components/icon-button';
import { useTheme } from '@hooks';

const StyledModal = styled.View`
  align-items: center;
  background-color: ${({ theme }) => theme.color.background};
  border-radius: 10px;
  elevation: 5;
  max-width: 400px;
  padding: 35px;
  width: 100%;
`;

const StyledModalBackground = styled(BlurView)`
  align-items: center;
  align-self: center;
  height: 100%;
  justify-content: center;
  padding-bottom: 10px;
  padding-left: 30px;
  padding-right: 30px;
  padding-top: 10px;
  width: 100%;
`;

const StyledCloseButton = styled(IconButton)`
  background-color: ${({ theme }) => theme.color.error.border};
`;

const StyledTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.large};
  margin-bottom: 10px;
`;

interface Props {
  children?: ReactNode;
  title: string;
}

const Modal = ({ children, title }: Props) => {
  const theme = useTheme();
  const navigation = useNavigation();
  const handleClose = () => {
    navigation.goBack();
  };

  return (
    <NativeModal animationType="slide" transparent visible>
      <StyledModalBackground intensity={80} tint="dark">
        <TouchableWithoutFeedback>
          <View
            style={{
              alignItems: 'center',
              width: '100%',
              flex: 1,
              paddingBottom: 40,
              paddingTop: 40,
            }}
          >
            <View style={{ flex: 1 }} />
            <StyledModal
              style={{
                elevation: 5,
                shadowColor: theme.color.shadowColor,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
              }}
            >
              <StyledTitle>{title}</StyledTitle>
              {children}
            </StyledModal>
            <View style={{ flex: 1 }} />
            <StyledCloseButton
              accessibilityLabel="cancel"
              icon="x"
              onPress={handleClose}
            />
          </View>
        </TouchableWithoutFeedback>
      </StyledModalBackground>
    </NativeModal>
  );
};

export default Modal;
