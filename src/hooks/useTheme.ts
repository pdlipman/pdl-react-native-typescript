import { useContext } from 'react';
import { ThemeContext } from 'styled-components/native';

const useTheme = () => {
  return useContext(ThemeContext);
};

export default useTheme;
