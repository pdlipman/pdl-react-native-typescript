import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';

/**
 * useFetching hook - dispatch action on component load
 * @param fetchActionCreator
 */
const useFetching = (fetchActionCreator: Action) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchActionCreator);
  }, []);
};

export default useFetching;
