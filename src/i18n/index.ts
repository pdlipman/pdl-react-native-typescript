import * as Localization from 'expo-localization';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import en from './locales/en/translation.json';

const resources = {
  en: {
    translation: en,
  },
};

i18n.use(initReactI18next).init({
  cleanCode: true,
  debug: process.env.NODE_ENV === 'development',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
  lng: Localization.locale,
  resources,
});

export default i18n;
