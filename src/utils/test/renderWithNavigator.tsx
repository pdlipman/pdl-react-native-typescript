import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { RenderOptions } from '@testing-library/react-native';
import React, { ReactElement } from 'react';

import { renderWithProviders } from '@utils/test/renderWithProviders';

interface MockedNavigatorProps {
  component: ReactElement;
}

interface RenderWithNavigatorProps {
  component: ReactElement;
  options?: RenderOptions;
}

const Stack = createStackNavigator();

export const MockedNavigator = ({
  component: Component,
}: MockedNavigatorProps) => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="MockedScreen">{() => Component}</Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export const renderWithNavigator = ({
  component,
  options,
}: RenderWithNavigatorProps) =>
  renderWithProviders({
    ui: <MockedNavigator component={component} />,
    options,
  });
