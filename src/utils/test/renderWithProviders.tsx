import { render, RenderOptions } from '@testing-library/react-native';
import React, { ReactElement } from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ThemeProvider } from 'styled-components/native';

import store, { persistor } from '@store';
import { themes } from '@styles/themes';

interface RenderWithProvidersProps {
  ui: ReactElement;
  options?: RenderOptions;
}

interface WrapperProps {
  children: ReactElement | ReactElement[];
}

const Wrapper = ({ children, ...props }: WrapperProps) => {
  const theme = themes('standard');
  return (
    <StoreProvider store={store}>
      <PersistGate persistor={persistor}>
        <ThemeProvider theme={theme}>{children}</ThemeProvider>
      </PersistGate>
    </StoreProvider>
  );
};

export const renderWithProviders = ({
  ui,
  options,
}: RenderWithProvidersProps) => render(ui, { wrapper: Wrapper, ...options });
