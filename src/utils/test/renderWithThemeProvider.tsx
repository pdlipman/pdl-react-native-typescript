import { render, RenderOptions } from '@testing-library/react-native';
import React, { ReactElement } from 'react';
import { ThemeProvider } from 'styled-components/native';

import { themes } from '@styles/themes';

interface RenderWithThemeProviderProps {
  ui: ReactElement;
  options?: RenderOptions;
}

interface WrapperProps {
  children: ReactElement | ReactElement[];
}

const Wrapper = ({ children, ...props }: WrapperProps) => {
  const theme = themes('standard');
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export const renderWithThemeProvider = ({
  ui,
  options,
}: RenderWithThemeProviderProps) =>
  render(ui, { wrapper: Wrapper, ...options });
