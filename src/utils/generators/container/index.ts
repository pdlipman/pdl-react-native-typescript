// const componentExists = require('../utils/componentExists');

const containerGenerator = {
  description: 'Create a container',
  // User input prompts provided as arguments to the template
  prompts: [
    {
      // Raw text input
      type: 'input',
      // Variable name for this input
      name: 'name',
      // Prompt to display on command line
      message: 'What is your container name?',
      // validate: (value) => {
      //   if (/.+/.test(value)) {
      //     return componentExists(value)
      //       ? 'A component or container with this name already exists'
      //       : true;
      //   }

      //   return 'The name is required';
      // },
    },
    {
      type: 'confirm',
      name: 'wantTests',
      default: true,
      message: 'Add tests?',
    },
    {
      type: 'confirm',
      name: 'wantRedux',
      default: false,
      message: 'Connect using Redux?',
    },
  ],
  actions: (data: any) => {
    const actions = [
      {
        // Add a new file
        type: 'add',
        // Path for the new file
        path: '../../../src/containers/{{dashCase name}}/index.tsx',
        // Handlebars template used to generate content of new file
        templateFile: './container/container-template.tsx.hbs',
        abortOnFail: true,
      },
    ];

    if (data.wantTests) {
      actions.push({
        type: 'add',
        path:
          '../../../src/containers/{{dashCase name}}/__tests__/index.spec.tsx',
        templateFile: './container/container-template.spec.tsx.hbs',
        abortOnFail: true,
      });
    }

    if (data.wantRedux) {
      actions.push({
        type: 'add',
        path: '../../../src/containers/{{dashCase name}}/redux/actions.ts',
        templateFile: './container/actions.ts.hbs',
        abortOnFail: true,
      });

      actions.push({
        type: 'add',
        path: '../../../src/containers/{{dashCase name}}/redux/constants.ts',
        templateFile: './container/constants.ts.hbs',
        abortOnFail: true,
      });

      actions.push({
        type: 'add',
        path: '../../../src/containers/{{dashCase name}}/redux/reducer.ts',
        templateFile: './container/reducer.ts.hbs',
        abortOnFail: true,
      });

      actions.push({
        type: 'add',
        path: '../../../src/containers/{{dashCase name}}/redux/selectors.ts',
        templateFile: './container/selectors.ts.hbs',
        abortOnFail: true,
      });

      actions.push({
        type: 'add',
        path: '../../../src/containers/{{dashCase name}}/redux/types.d.ts',
        templateFile: './container/types.d.ts.hbs',
        abortOnFail: true,
      });
    }

    actions.push({
      type: 'prettify',
      path: 'src/containers/',
      templateFile: '',
      abortOnFail: true,
    });

    return actions;
  },
};

export default containerGenerator;
