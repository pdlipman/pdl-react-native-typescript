import { execSync } from 'child_process';
import { NodePlopAPI } from 'plop';

import componentGenerator from './component';
import containerGenerator from './container';
import styledComponentGenerator from './styled-component';

export default function (plop: NodePlopAPI): void {
  // plop generator code
  plop.setGenerator('component', componentGenerator);
  plop.setGenerator('container', containerGenerator);
  plop.setGenerator('styled-component', styledComponentGenerator);

  plop.setActionType('prettify', (answers: any, config: any) => {
    const { path } = config;
    const folderPath = `${path}${plop.getHelper('dashCase')(
      answers.name
    )}/**/**.{ts,tsx}`;
    execSync(`yarn prettier --write "${folderPath}"`);
    return folderPath;
  });
}
