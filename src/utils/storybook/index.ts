import { action } from '@storybook/addon-actions';

import CenterView from './center-view';

const withAction = (actionName: string) => {
  const beacon = action(actionName);
  return (eventObj: any, ...args: any[]) => {
    beacon({ ...eventObj, view: undefined }, ...args);
  };
};

export { CenterView, withAction };
