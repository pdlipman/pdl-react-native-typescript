import React, { ReactNode } from 'react';
import styled from 'styled-components/native';

const StyledView = styled.View`
  align-items: center;
  background-color: #f5fcff;
  flex: 1;
  flex-wrap: wrap;
  justify-content: center;
  padding: 10px;
`;

interface Props {
  children: ReactNode;
}

const CenterView = ({ children }: Props) => {
  return <StyledView>{children}</StyledView>;
};

export default CenterView;
