import React from 'react';

import StyledForm from '@styled-components/styled-form';
import { renderWithThemeProvider } from '@utils/test';

describe('<StyledForm />', () => {
  it('renders correctly', () => {
    const styledComponent = renderWithThemeProvider({ ui: <StyledForm /> });
    expect(styledComponent).toMatchSnapshot();
  });
});
