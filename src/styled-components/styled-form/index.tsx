import React, { ReactNode } from 'react';
import { Keyboard, TouchableWithoutFeedback } from 'react-native';
import styled from 'styled-components/native';

import { useKeyboardVisible } from '@hooks';

const StyledWrapper = styled.View`
  max-width: 400px;
  width: 100%;
`;

interface Props {
  children?: ReactNode;
}

const StyledForm = ({ children }: Props) => {
  const isKeyboardVisible = useKeyboardVisible();

  return (
    <TouchableWithoutFeedback
      accessible={false}
      disabled={!isKeyboardVisible}
      onPress={Keyboard.dismiss}
    >
      <StyledWrapper>{children}</StyledWrapper>
    </TouchableWithoutFeedback>
  );
};
export default StyledForm;
