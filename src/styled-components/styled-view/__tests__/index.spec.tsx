import React from 'react';

import StyledView from '@styled-components/styled-view';
import { renderWithThemeProvider } from '@utils/test';

describe('<StyledView />', () => {
  it('renders correctly', () => {
    const styledComponent = renderWithThemeProvider({ ui: <StyledView /> });
    expect(styledComponent).toMatchSnapshot();
  });
});
