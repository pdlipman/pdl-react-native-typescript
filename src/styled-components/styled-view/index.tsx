import React, { ReactNode } from 'react';
import { ViewProps } from 'react-native';
import styled from 'styled-components/native';

const StyledWrapper = styled.View`
  align-items: center;
  align-self: center;
  background-color: ${({ theme }) => theme.color.background};
  height: 100%;
  justify-content: center;
  padding-bottom: 20px;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;
  width: 100%;
`;

interface Props extends ViewProps {
  children?: ReactNode;
  className?: string;
}

const StyledView = ({ children, style }: Props) => {
  return <StyledWrapper style={style}>{children}</StyledWrapper>;
};

export default StyledView;
