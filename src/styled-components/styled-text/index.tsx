import styled from 'styled-components/native';

const StyledText = styled.Text`
  color: ${({ theme }) => theme.color.font};
  font-size: ${({ theme }) => theme.fontSizes.small};
`;

export default StyledText;
