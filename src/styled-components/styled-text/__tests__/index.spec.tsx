import { render } from '@testing-library/react-native';
import React from 'react';

import StyledText from '@styled-components/styled-text';
import { renderWithThemeProvider } from '@utils/test';

describe('<StyledText />', () => {
  it('renders correctly', () => {
    const styledComponent = renderWithThemeProvider({ ui: <StyledText /> });
    expect(styledComponent).toMatchSnapshot();
  });
});
