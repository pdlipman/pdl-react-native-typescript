import React from 'react';

import UserButton from '@containers/user-button';
import { renderWithNavigator } from '@utils/test/renderWithNavigator';

describe('<UserButton />', () => {
  it('renders correctly', () => {
    const view = renderWithNavigator({ component: <UserButton /> });
    const { getByTestId } = view;
    expect(getByTestId('user-button-container')).toBeTruthy();
  });
});
