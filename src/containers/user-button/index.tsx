import { useNavigation } from '@react-navigation/native';
import React, { useRef, useState } from 'react';
import {
  Animated,
  InteractionManager,
  LayoutChangeEvent,
  View,
} from 'react-native';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';

import IconButton from '@components/icon-button';
import i18n from '@i18n';
import { selectUid } from '@providers/firebase/auth/selectors';

const StyledButtonFill = styled(Animated.View)`
  background-color: ${({ theme }) => theme.color.primary.background};
  position: absolute;
  border-radius: ${({ theme }) => theme.borderRadius};
  top: 0;
  left: 0;
`;

const UserButton = () => {
  const [buttonSize, setButtonSize] = useState({ width: 0, height: 0 });
  const navigation = useNavigation();
  const fadeInValue = useRef(new Animated.Value(0)).current;
  const scaleValue = useRef(new Animated.Value(0)).current;
  const uid = useSelector(selectUid);

  const handleLayout = (event: LayoutChangeEvent) => {
    const { width, height } = event.nativeEvent.layout;
    setButtonSize({ width, height });
  };

  const handlePress = () => {
    if (uid) {
      Animated.parallel([
        Animated.timing(fadeInValue, {
          toValue: 1,
          useNativeDriver: true,
          duration: 100,
        }),
        Animated.timing(scaleValue, {
          toValue: 1,
          useNativeDriver: true,
          duration: 700,
        }),
      ]).start(() => {
        fadeInValue.setValue(0);
        scaleValue.setValue(0);
      });
    }

    InteractionManager.runAfterInteractions(() => {
      if (uid) {
        navigation.navigate('UserProfile');
      } else {
        navigation.navigate('Login');
      }
    });
  };

  const scaleValueInterpolation = scaleValue.interpolate({
    inputRange: [0, 0.25, 1],
    outputRange: [1, 20, 30],
  });

  return (
    <View>
      <StyledButtonFill
        style={{
          transform: [{ scale: scaleValueInterpolation }],
          ...buttonSize,
        }}
      />
      <IconButton
        accessibilityLabel={i18n.t('userProfile', '[User Profile]')}
        icon={uid ? 'user' : 'log-in'}
        onPress={handlePress}
        onLayout={handleLayout}
      />
    </View>
  );
};

export default UserButton;
