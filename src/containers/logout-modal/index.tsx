import React from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components/native';

import { Primary } from '@components/button';
import Modal from '@components/modal';
import { logoutAction } from '@providers/firebase/login/actions';

const StyledLogoutModal = styled.View`
  width: 100%;
`;

const Logout = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logoutAction());
  };

  return (
    <Modal title="Logout">
      <StyledLogoutModal>
        <Primary onPress={handleLogout} title="Logout" />
      </StyledLogoutModal>
    </Modal>
  );
};

export default Logout;
