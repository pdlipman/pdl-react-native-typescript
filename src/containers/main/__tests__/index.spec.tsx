import { fireEvent, waitFor } from '@testing-library/react-native';
import React from 'react';

import Main from '@containers/main';
import { renderWithProviders } from '@utils/test/renderWithProviders';

describe('Main', () => {
  it('renders correctly', async () => {
    const { getByText } = renderWithProviders({ ui: <Main /> });

    await waitFor(() => {
      expect(getByText(/Home Header/i)).toBeTruthy();
    });
  });

  it('displays login screen', async () => {
    const { getByText, getByLabelText } = renderWithProviders({ ui: <Main /> });

    await waitFor(() => {
      fireEvent.press(getByLabelText(/Login to app/i));
      expect(getByText(/Login Header/i)).toBeTruthy();
    });
  });
});
