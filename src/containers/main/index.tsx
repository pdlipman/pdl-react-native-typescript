import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Platform, StatusBar } from 'react-native';
import { useSelector } from 'react-redux';
import styled, { ThemeProvider } from 'styled-components/native';

import Home from '@containers/home';
import Login from '@containers/login';
import Logout from '@containers/logout-modal';
import Posts from '@containers/posts';
import Register from '@containers/register';
import UserProfile from '@containers/user-profile';
import {
  selectIsAdmin,
  selectTheme,
  selectUid,
} from '@providers/firebase/auth/selectors';
import { themes } from '@styles/themes';

import StoryBook from '../../../storybook';

const StyledApp = styled.KeyboardAvoidingView`
  align-items: center;
  align-self: center;
  background-color: ${(props) => props.theme.color.background};
  height: 100%;
  width: 100%;
`;

const StyledMain = styled.SafeAreaView`
  flex: 1;
  max-width: 768px;
  width: 100%;
`;

const Main = () => {
  const isAdmin = useSelector(selectIsAdmin);
  const userTheme = useSelector(selectTheme);
  const uid = useSelector(selectUid);

  const RootStack = createStackNavigator();
  const MainStack = createStackNavigator();
  const theme = themes(userTheme);

  const generateAuthStack = () => {
    if (uid) {
      return (
        <>
          <RootStack.Screen name="UserProfile" component={UserProfile} />
          <RootStack.Screen
            name="Logout"
            component={Logout}
            options={{ headerShown: false }}
          />
        </>
      );
    }
  };

  const generateRegisterStack = () => {
    if (!uid) {
      return (
        <>
          <MainStack.Screen name="Login" component={Login} />
          <MainStack.Screen name="Register" component={Register} />
        </>
      );
    }
  };

  const opacityTransition: object = {
    gestureDirection: 'horizontal',
    transitionSpec: {
      open: {
        animation: 'timing',
      },
      close: {
        animation: 'timing',
        config: {
          duration: 300,
        },
      },
    },
    cardStyleInterpolator: ({
      current,
    }: {
      current: { progress: number };
    }) => ({
      cardStyle: {
        opacity: current.progress,
      },
    }),
  };

  const header = {
    headerStyle: { backgroundColor: theme.color.background },
    // headerTintColor: theme.color.font,
    headerTitleStyle: { color: theme.color.font },
  };

  const MainStackScreen = () => {
    return (
      <MainStack.Navigator
        screenOptions={{
          ...header,
        }}
      >
        <MainStack.Screen name="Home" component={Home} />
        <MainStack.Screen name="Posts" component={Posts} />
        {isAdmin && <MainStack.Screen name="StoryBook" component={StoryBook} />}
        {generateRegisterStack()}
      </MainStack.Navigator>
    );
  };

  return (
    <ThemeProvider theme={theme}>
      <StyledApp behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <StatusBar
          backgroundColor={theme.color.background}
          barStyle={theme.color.statusBarContent}
        />
        <StyledMain>
          <NavigationContainer>
            <RootStack.Navigator
              mode="modal"
              screenOptions={{
                ...opacityTransition,
                ...header,
                cardStyle: { backgroundColor: 'transparent' },
              }}
            >
              <RootStack.Screen
                name="Main"
                component={MainStackScreen}
                options={{ headerShown: false }}
              />
              {generateAuthStack()}
            </RootStack.Navigator>
          </NavigationContainer>
        </StyledMain>
      </StyledApp>
    </ThemeProvider>
  );
};
export default Main;
