import { createSelector } from 'reselect';

import { ApplicationState } from '@state';

export const mainSelector = (state: ApplicationState) => state.main;
export const statusSelector = createSelector(
  mainSelector,
  (main) => main.status
);
