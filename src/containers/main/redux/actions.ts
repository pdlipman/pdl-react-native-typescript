import { createActionWithPayload } from '@utils/redux/createAction';

import ActionTypes from './constants';

export const applicationLoaded = createActionWithPayload<string>(
  ActionTypes.APPLICATION_LOADED
);
