export interface MainState {
  readonly status: string;
}

type ContainerState = MainState;

export { ContainerState };
