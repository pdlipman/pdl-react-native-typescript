import { createReducer } from '@reduxjs/toolkit';

import { applicationLoaded } from './actions';
import { ContainerState } from './types';

const initialState: ContainerState = {
  status: 'main initial',
};

export const mainReducer = createReducer(initialState, (builder) => {
  return builder.addCase(applicationLoaded, (state, { payload }) => ({
    ...state,
    status: payload,
  }));
});
