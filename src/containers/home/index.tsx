import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';

import { Primary } from '@components/button';
import StorybookButton from '@containers/storybook-button';
import UserButton from '@containers/user-button';
import { useFetching } from '@hooks';
import i18n from '@i18n';
import { postsRequestAction } from '@providers/firebase/posts/actions';
import { StyledText, StyledView } from '@styled-components';

import { homeLoaded } from './redux/actions';
import { statusSelector } from './redux/selectors';

const StyledWrapper = styled.View`
  align-items: center;
  align-self: center;
  flex: 1;
  justify-content: center;
  max-width: 400px;
  width: 100%;
`;

const Home = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const status = useSelector(statusSelector);

  useEffect(() => {
    const status = i18n.t('reduxLoaded', '[Redux Loaded]');
    dispatch(homeLoaded(status));
  }, []);

  useFetching(postsRequestAction());

  return (
    <StyledView>
      <StyledWrapper>
        <StyledText>React Native Typescript: {status}</StyledText>
        <StyledText>{i18n.t('homeHeader', '[Home Header]')}</StyledText>
        <Primary
          accessibilityLabel={i18n.t('viewPosts', '[View Posts]')}
          onPress={() => navigation.navigate('Posts')}
          title={i18n.t('viewPosts', '[View Posts]')}
        />
      </StyledWrapper>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
        }}
      >
        <View>
          <StorybookButton />
        </View>
        <View style={{ flex: 1 }} />
        <View>
          <UserButton />
        </View>
      </View>
    </StyledView>
  );
};

export default Home;
