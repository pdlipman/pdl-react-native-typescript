import { createReducer } from '@reduxjs/toolkit';

import { homeLoaded } from './actions';
import { ContainerState } from './types';

const initialState: ContainerState = {
  status: 'home initial',
};

export const homeReducer = createReducer(initialState, (builder) => {
  return builder.addCase(homeLoaded, (state, { payload }) => ({
    ...state,
    status: payload,
  }));
});
