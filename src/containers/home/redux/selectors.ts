import { createSelector } from 'reselect';

import { ApplicationState } from '@state';

export const homeSelector = (state: ApplicationState) => state.home;
export const statusSelector = createSelector(
  homeSelector,
  (home) => home.status
);
