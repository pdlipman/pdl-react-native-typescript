import { createActionWithPayload } from '@utils/redux/createAction';

import ActionTypes from './constants';

export const homeLoaded = createActionWithPayload<string>(
  ActionTypes.HOME_LOADED
);
