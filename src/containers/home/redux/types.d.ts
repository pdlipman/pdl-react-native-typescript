export interface HomeState {
  readonly status: string;
}

type ContainerState = HomeState;

export { ContainerState };
