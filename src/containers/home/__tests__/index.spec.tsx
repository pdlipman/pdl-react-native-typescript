import { fireEvent, waitFor } from '@testing-library/react-native';
import React from 'react';

import Home from '@containers/home';
import { renderWithNavigator } from '@utils/test/renderWithNavigator';

const mockedNavigate = jest.fn();
jest.mock('@react-navigation/native', () => {
  const originalModule = jest.requireActual('@react-navigation/native');
  return {
    ...originalModule,
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
  };
});

describe('<Home />', () => {
  it('renders correctly', async () => {
    const { getByText } = renderWithNavigator({
      component: <Home />,
    });
    const page = await waitFor(() => getByText(/React Native Typescript:/i));
    expect(page).toBeTruthy();
  });

  it('navigates to login', async () => {
    const { getByText } = renderWithNavigator({
      component: <Home />,
    });
    const submit = getByText(/Login/i);
    await waitFor(() => fireEvent.press(submit));
    expect(mockedNavigate).toHaveBeenCalledWith('Login');
  });
});
