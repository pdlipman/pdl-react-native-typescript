import { fireEvent, waitFor } from '@testing-library/react-native';
import React from 'react';

import Login from '@containers/login';
import * as loginActions from '@providers/firebase/login/actions';
import { PASSWORD_MIN_LENGTH } from '@utils/constants';
import { renderWithNavigator } from '@utils/test/renderWithNavigator';

describe('<Login />', () => {
  it('renders correctly', async () => {
    const { getByLabelText, getByText } = renderWithNavigator({
      component: <Login />,
    });

    await waitFor(() => {
      getByText(/Login Header/i);
    });

    const email = getByLabelText(/Email/i);
    const password = getByLabelText(/^Password$/i);
    const submit = getByLabelText(/Login to app/i);
    expect(email).toBeTruthy();
    expect(password).toBeTruthy();
    expect(submit).toBeTruthy();
  });

  it('email is required', async () => {
    const { getByLabelText, getByText } = renderWithNavigator({
      component: <Login />,
    });

    await waitFor(() => {
      getByText(/Login Header/i);
    });

    const email = getByLabelText(/Email/i);
    const submit = getByLabelText(/Login to app/i);

    fireEvent.changeText(email, '');
    fireEvent.press(submit);

    await waitFor(() => {
      expect(getByText(/Required/i)).toBeTruthy();
    });
  });

  it('well formatted email is required', async () => {
    const { getByLabelText, getByText } = renderWithNavigator({
      component: <Login />,
    });

    await waitFor(() => {
      getByText(/Login Header/i);
    });

    const email = getByLabelText(/Email/i);
    const password = getByLabelText(/^Password$/i);
    const submit = getByLabelText(/Login to app/i);

    const badEmail = 'bad email';
    fireEvent.changeText(email, badEmail);
    fireEvent.changeText(password, 'password');
    fireEvent.press(submit);

    await waitFor(() => {
      expect(getByText(/Invalid Email/i)).toBeTruthy();
    });
  });

  it(`password greater than or equal to ${PASSWORD_MIN_LENGTH} is required`, async () => {
    const { getByLabelText, getByText } = renderWithNavigator({
      component: <Login />,
    });

    await waitFor(() => {
      getByText(/Login Header/i);
    });

    const password = getByLabelText(/^Password$/i);
    const submit = getByLabelText(/Login to app/i);

    const badPassword = '123';
    fireEvent.changeText(password, badPassword);
    fireEvent(password, 'blur');
    fireEvent.press(submit);

    await waitFor(() => {
      expect(
        getByText(
          new RegExp(
            `Password should be at least ${PASSWORD_MIN_LENGTH} characters`
          )
        )
      ).toBeTruthy();
    });
  });

  it('form is submitted', async () => {
    const loginRequestActionSpy = jest.spyOn(
      loginActions,
      'loginRequestAction'
    );

    const { getByLabelText, getByText } = renderWithNavigator({
      component: <Login />,
    });

    await waitFor(() => {
      getByText(/Login Header/i);
    });

    const email = getByLabelText(/Email/i);
    const password = getByLabelText(/^Password$/i);
    const submit = getByLabelText(/Login to app/i);

    const goodEmail = 'login.good@email.com';
    const goodPassword = 'HelloWorld1234!';
    fireEvent.changeText(email, goodEmail);
    fireEvent.changeText(password, goodPassword);
    fireEvent.press(submit);
    await waitFor(() => {
      expect(getByText(new RegExp(`"email":"${goodEmail}"`))).toBeTruthy();
      expect(
        getByText(new RegExp(`"password":"${goodPassword}"`))
      ).toBeTruthy();
    });
    expect(loginRequestActionSpy).toHaveBeenCalledWith({
      email: goodEmail,
      password: goodPassword,
    });
  });
});
