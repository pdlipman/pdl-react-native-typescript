import { yupResolver } from '@hookform/resolvers';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Keyboard, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';

import { Primary, Secondary } from '@components/button';
import { Email, Password } from '@components/input';
import i18n from '@i18n';
import {
  loginInitializeAction,
  loginRequestAction,
} from '@providers/firebase/login/actions';
import StyledForm from '@styled-components/styled-form';
import StyledView from '@styled-components/styled-view';
import { PASSWORD_MIN_LENGTH } from '@utils/constants';

import Error from './error';

const loginSchema = yup.object().shape({
  email: yup
    .string()
    .email(i18n.t('invalidEmail', 'Invalid Email'))
    .required(i18n.t('required', 'Required')),
  password: yup
    .string()
    .min(
      PASSWORD_MIN_LENGTH,
      i18n.t(
        'minPasswordLength',
        'Password should be at least {{minLength}} characters',
        {
          minLength: PASSWORD_MIN_LENGTH,
        }
      )
    )
    .required(i18n.t('required', 'Required')),
});

interface FormData {
  email: string;
  password: string;
}

const Login = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [submitValues, setSubmitValues] = useState({});

  const { control, errors, handleSubmit } = useForm<FormData>({
    mode: 'onTouched',
    resolver: yupResolver(loginSchema),
  });

  const handleRegister = () => {
    const routes = [{ name: 'Home' }, { name: 'Register' }];
    navigation.reset({
      routes,
      index: routes.length - 1,
    });
    Keyboard.dismiss();
  };

  const onSubmit = handleSubmit(({ email, password }) => {
    setSubmitValues({ email, password });
    dispatch(loginRequestAction({ email, password }));
    Keyboard.dismiss();
  });

  // initialize login
  useEffect(() => {
    dispatch(loginInitializeAction());
  }, []);

  return (
    <StyledView>
      <StyledForm>
        <Error />
        <Controller
          control={control}
          render={({ onChange, onBlur, value }) => (
            <Email
              error={Boolean(errors?.email)}
              helperText={errors?.email?.message}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="email"
          defaultValue=""
        />

        <Controller
          control={control}
          render={({ onChange, onBlur, value }) => (
            <Password
              error={Boolean(errors?.password)}
              helperText={errors?.password?.message}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="password"
          defaultValue=""
        />
        <Primary
          label={i18n.t('loginToApp', '[Login to app]')}
          onPress={onSubmit}
          title={i18n.t('loginToApp', '[Login to app]')}
        />
        <View>
          <Text>{JSON.stringify(submitValues)}</Text>
        </View>
        <View>
          <Text>Not a member?</Text>
          <Secondary
            accessibilityLabel={i18n.t(
              'registerNewAccount',
              '[Register new account]'
            )}
            onPress={handleRegister}
            title={i18n.t('registerANewAccount', '[Register a new account]')}
          />
        </View>
      </StyledForm>
    </StyledView>
  );
};

export default Login;
