import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useSelector } from 'react-redux';

import IconButton from '@components/icon-button';
import i18n from '@i18n';
import { selectIsAdmin } from '@providers/firebase/auth/selectors';

const StorybookButton = () => {
  const isAdmin = useSelector(selectIsAdmin);
  const navigation = useNavigation();
  const handlePress = () => {
    navigation.navigate('StoryBook');
  };

  if (!isAdmin) {
    return null;
  }

  return (
    <IconButton
      accessibilityLabel={i18n.t('storyBook', '[StoryBook]')}
      icon="book-open"
      onPress={handlePress}
      title={i18n.t('storyBook', '[StoryBook]')}
    />
  );
};

export default StorybookButton;
