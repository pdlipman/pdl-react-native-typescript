import React from 'react';

import StorybookButton from '@containers/storybook-button';
import { renderWithNavigator } from '@utils/test/renderWithNavigator';

describe('<StorybookButton />', () => {
  it('renders correctly', () => {
    const view = renderWithNavigator({ component: <StorybookButton /> });
    const { getByLabelText } = view;
    expect(getByLabelText('StoryBook')).toBeTruthy();
  });
});
