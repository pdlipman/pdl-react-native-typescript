import { yupResolver } from '@hookform/resolvers';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Keyboard, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';

import { Secondary } from '@components/button';
import { Email, Password } from '@components/input';
import i18n from '@i18n';
import {
  registerInitializeAction,
  registerRequestAction,
} from '@providers/firebase/register/actions';
import StyledForm from '@styled-components/styled-form';
import StyledView from '@styled-components/styled-view';
import { PASSWORD_MIN_LENGTH } from '@utils/constants';

import Error from './error';

const registerSchema = yup.object().shape({
  email: yup
    .string()
    .email(i18n.t('invalidEmail', 'Invalid Email'))
    .required(i18n.t('required', 'Required')),
  password: yup
    .string()
    .min(
      PASSWORD_MIN_LENGTH,
      i18n.t(
        'minPasswordLength',
        'Password should be at least {{minLength}} characters',
        {
          minLength: PASSWORD_MIN_LENGTH,
        }
      )
    )
    .required(i18n.t('required', 'Required')),
});

interface FormData {
  email: string;
  password: string;
}

const Register = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [submitValues, setSubmitValues] = useState({});

  const { control, errors, handleSubmit } = useForm<FormData>({
    mode: 'onTouched',
    resolver: yupResolver(registerSchema),
  });

  const handleLogin = () => {
    navigation.navigate('Login');
    Keyboard.dismiss();
  };

  const onSubmit = handleSubmit(({ email, password }) => {
    setSubmitValues({ email, password });
    dispatch(registerRequestAction({ email, password }));
    Keyboard.dismiss();
  });

  // initialize register
  useEffect(() => {
    dispatch(registerInitializeAction());
  }, []);

  return (
    <StyledView>
      <StyledForm>
        <Error />
        <Controller
          control={control}
          render={({ onChange, onBlur, value }) => (
            <Email
              error={Boolean(errors?.email)}
              helperText={errors?.email?.message}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="email"
          defaultValue=""
        />

        <Controller
          control={control}
          render={({ onChange, onBlur, value }) => (
            <Password
              error={Boolean(errors?.password)}
              helperText={errors?.password?.message}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="password"
          defaultValue=""
        />
        <Secondary
          label={i18n.t('registerNewAccount', '[Register new account]')}
          onPress={onSubmit}
          title={i18n.t('registerNewAccount', '[Register new account]')}
        />
        <View>
          <Text>{JSON.stringify(submitValues)}</Text>
        </View>
      </StyledForm>
    </StyledView>
  );
};

export default Register;
