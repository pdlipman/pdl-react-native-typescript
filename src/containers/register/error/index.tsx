import React from 'react';
import { useSelector } from 'react-redux';

import { Error as ErrorMessage } from '@components/message';
import {
  selectErrorCode,
  selectErrorMessage,
} from '@providers/firebase/register/selectors';

const Error = () => {
  const code = useSelector(selectErrorCode);
  const message = useSelector(selectErrorMessage);
  if (code && message) {
    return <ErrorMessage header={code} message={message} />;
  }
  return null;
};

export default Error;
