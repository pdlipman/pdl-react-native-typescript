import React from 'react';

import Posts from '@containers/posts';
import { renderWithNavigator } from '@utils/test/renderWithNavigator';

describe('<Posts />', () => {
  it('renders correctly', () => {
    const view = renderWithNavigator({ component: <Posts /> });
    const { getByTestId } = view;
    expect(getByTestId('posts-container')).toBeTruthy();
  });
});
