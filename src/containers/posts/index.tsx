import React from 'react';
import { FlatList, ImageBackground, Text, View } from 'react-native';
import { useSelector } from 'react-redux';

import i18n from '@i18n';
import { selectAllPosts } from '@providers/firebase/posts/selectors';
// import { Post } from '@providers/firebase/posts/types';

interface Props {
  coverImage: string;
  slug: string;
  title: string;
}

const PostThumbnail = ({ coverImage, slug, title }: Props) => {
  //   console.log(`coverImage: ${coverImage}`);
  //   console.log(`slug: ${slug}`);
  //   console.log(`title: ${title}`);
  return (
    <View>
      <ImageBackground
        source={{ uri: coverImage }}
        style={{
          flex: 1,
          justifyContent: 'center',
          width: 150,
          height: 150,
        }}
      >
        <Text>{title}</Text>
      </ImageBackground>
    </View>
  );
};

const Posts = () => {
  const posts = useSelector(selectAllPosts);
  const handleRenderItem = ({ item }: any) => {
    const { coverImage, slug, title } = item;
    return <PostThumbnail coverImage={coverImage} slug={slug} title={title} />;
  };
  return (
    <View testID="posts-container">
      <Text>{i18n.t('posts', '[Posts]')}</Text>
      <FlatList
        data={posts}
        renderItem={handleRenderItem}
        keyExtractor={(item) => item.slug}
      />
    </View>
  );
};

export default Posts;
