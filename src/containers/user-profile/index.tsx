import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';

import { Secondary } from '@components/button';
import i18n from '@i18n';
import {
  selectIsAdmin,
  selectTheme,
  selectUid,
} from '@providers/firebase/auth/selectors';
import { StyledText, StyledView } from '@styled-components';

const UserProfileView = styled(StyledView)`
  background-color: ${({ theme }) => theme.color.primary.background};
`;

const UserProfile = () => {
  const navigation = useNavigation();
  const isAdmin = useSelector(selectIsAdmin);
  const userTheme = useSelector(selectTheme);
  const uid = useSelector(selectUid);

  const showLogout = () => {
    navigation.navigate('Logout');
  };

  return (
    <UserProfileView>
      <StyledText>Hello World 1</StyledText>
      <StyledText>{i18n.t('userProfile', '[User Profile]')}</StyledText>
      <StyledText>{`uid: ${uid}`}</StyledText>
      <StyledText>{`isAdmin: ${isAdmin}`}</StyledText>
      <StyledText>{`Current Theme: ${userTheme}`}</StyledText>
      <Secondary
        accessibilityLabel={i18n.t('logoutOfApp', '[Logout of app]')}
        onPress={showLogout}
        title={i18n.t('logoutOfApp', '[Logout of app]')}
      />
    </UserProfileView>
  );
};

export default UserProfile;
