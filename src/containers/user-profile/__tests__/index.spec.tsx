import React from 'react';

import UserProfile from '@containers/user-profile';
import { renderWithNavigator } from '@utils/test/renderWithNavigator';

describe('<UserProfile />', () => {
  it('renders correctly', () => {
    const view = renderWithNavigator({ component: <UserProfile /> });
    const { getByTestId } = view;
    expect(getByTestId('user-profile-container')).toBeTruthy();
  });
});
