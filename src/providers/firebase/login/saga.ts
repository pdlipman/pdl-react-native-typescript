import {
  call,
  cancelled,
  fork,
  put,
  take,
  takeLatest,
} from 'redux-saga/effects';

import firebase from '@providers/firebase';

import {
  loginCancelledAction,
  loginErrorAction,
  loginSuccessAction,
} from './actions';
import ActionTypes from './constants';

function* login(email: string, password: string) {
  try {
    const auth = firebase.auth();
    yield call([auth, auth.signInWithEmailAndPassword], email, password);

    yield put(loginSuccessAction());
  } catch (error) {
    const { code, message } = error;
    yield put(loginErrorAction({ error: { code, message } }));
  } finally {
    if (yield cancelled()) {
      yield put(loginCancelledAction());
    }
  }
}

function* loginFlow() {
  while (true) {
    const { payload } = yield take(ActionTypes.LOGIN_REQUEST);
    const { email, password } = payload;

    yield fork(login, email, password);
  }
}

function* logout() {
  try {
    const auth = firebase.auth();
    yield call([auth, auth.signOut]);
  } catch (error) {
    yield put(loginErrorAction({ error }));
  }
}

function* logoutFlow() {
  yield takeLatest(ActionTypes.LOGOUT, logout);
}

export default function* loginSaga() {
  yield call(console.log, 'Register sagas loaded.');
  yield fork(loginFlow);
  yield fork(logoutFlow);
}
