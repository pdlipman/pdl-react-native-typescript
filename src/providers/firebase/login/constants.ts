enum ActionTypes {
  LOGIN_CANCELLED = 'providers/firebase/login/LOGIN_CANCELLED',
  LOGIN_ERROR = 'providers/firebase/login/LOGIN_ERROR',
  LOGIN_INITIALIZE = 'providers/firebase/login/LOGIN_INITIALIZE',
  LOGIN_REQUEST = 'providers/firebase/login/LOGIN_REQUEST',
  LOGIN_SUCCESS = 'providers/firebase/login/LOGIN_SUCCESS',
  LOGOUT = 'providers/firebase/login/LOGOUT',
}

export default ActionTypes;
