import { Error } from '@providers/firebase/types';

export interface LoginState {
  readonly error?: Error;
  readonly status: string;
}

type ContainerState = LoginState;
export { ContainerState };
