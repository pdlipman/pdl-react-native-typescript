import { createSelector } from 'reselect';

import { ApplicationState } from '@state';

const selectLogin = (state: ApplicationState) => state.login;
const selectError = createSelector(selectLogin, (login) => login.error);

export const selectErrorCode = createSelector(
  selectError,
  (error) => error?.code
);
export const selectErrorMessage = createSelector(
  selectError,
  (error) => error?.message
);

const selectStatus = createSelector(selectLogin, (login) => login.status);
