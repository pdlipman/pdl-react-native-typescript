import { createReducer } from '@reduxjs/toolkit';

import {
  loginCancelledAction,
  loginErrorAction,
  loginInitializeAction,
  loginRequestAction,
  loginSuccessAction,
  logoutAction,
} from './actions';
import { ContainerState } from './types';

const initialState: ContainerState = {
  error: { code: '', message: '' },
  status: 'login initial',
};

const loginReducer = createReducer(initialState, (builder) => {
  return builder
    .addCase(loginCancelledAction, (state) => ({
      ...state,
      error: initialState.error,
      status: 'login cancelled',
    }))
    .addCase(loginErrorAction, (state, { payload }) => {
      const { error } = payload;
      return { ...state, error, status: 'login error' };
    })
    .addCase(loginInitializeAction, () => ({
      ...initialState,
    }))
    .addCase(loginRequestAction, (state) => ({
      ...state,
      error: initialState.error,
      status: 'login request',
    }))
    .addCase(loginSuccessAction, (state) => ({
      ...state,
      error: initialState.error,
      status: 'login success',
    }))
    .addCase(logoutAction, (state) => ({ ...state, status: 'logout' }));
});

export default loginReducer;
