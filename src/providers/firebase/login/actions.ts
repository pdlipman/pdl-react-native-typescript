import { Error } from '@providers/firebase/types';
import {
  createAction,
  createActionWithPayload,
} from '@utils/redux/createAction';

import ActionTypes from './constants';

export const loginCancelledAction = createAction(ActionTypes.LOGIN_CANCELLED);

export const loginErrorAction = createActionWithPayload<{
  error: Error;
}>(ActionTypes.LOGIN_ERROR);

export const loginInitializeAction = createAction(ActionTypes.LOGIN_INITIALIZE);

export const loginRequestAction = createActionWithPayload<{
  email: string;
  password: string;
}>(ActionTypes.LOGIN_REQUEST);

export const loginSuccessAction = createAction(ActionTypes.LOGIN_SUCCESS);
export const logoutAction = createAction(ActionTypes.LOGOUT);
