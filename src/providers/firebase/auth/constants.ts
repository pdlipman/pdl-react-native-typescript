enum ActionTypes {
  AUTH_STATE_CHANGED = 'providers/firebase/auth/AUTH_STATE_CHANGED',
  AUTH_USER_AVAILABLE = 'providers/firebase/auth/AUTH_USER_AVAILABLE',
  AUTH_USER_NEW_CLAIMS_AVAILABLE = 'providers/firebase/auth/AUTH_USER_NEW_CLAIMS_AVAILABLE',
  AUTH_USER_UNAVAILABLE = 'providers/firebase/auth/AUTH_USER_UNAVAILABLE',
}

export default ActionTypes;
