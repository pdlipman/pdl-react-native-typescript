import {
  createAction,
  createActionWithPayload,
} from '@utils/redux/createAction';

import ActionTypes from './constants';
import { Claims, User } from './types';

export const authStateChangedAction = createAction(
  ActionTypes.AUTH_STATE_CHANGED
);

export const authUserAvailableAction = createActionWithPayload<{
  claims: Claims;
  user: User;
}>(ActionTypes.AUTH_USER_AVAILABLE);

export const authUserNewClaimsAvailableAction = createActionWithPayload<{
  claims: any;
}>(ActionTypes.AUTH_USER_NEW_CLAIMS_AVAILABLE);

export const authUserUnavailableAction = createAction(
  ActionTypes.AUTH_USER_UNAVAILABLE
);
