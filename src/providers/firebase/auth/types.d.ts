import { ThemeStyle } from '@styles/themes';
export interface Claims {
  isAdmin: boolean;
  theme: ThemeStyle;
}

export interface User {
  uid?: string;
}

export interface AuthState {
  readonly claims: Claims;
  readonly status: string;
  readonly user: User;
}

type ContainerState = AuthState;
export { ContainerState };
