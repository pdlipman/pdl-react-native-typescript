import { eventChannel } from 'redux-saga';
import { call, fork, put, select, take, takeLatest } from 'redux-saga/effects';

import firebase from '@providers/firebase';

import {
  authUserAvailableAction,
  authUserNewClaimsAvailableAction,
  authUserUnavailableAction,
} from './actions';
import ActionTypes from './constants';
import { selectUser } from './selectors';

function createAuthChannel() {
  const authChannel = eventChannel((emit) => {
    const unsubscribe = firebase.auth().onAuthStateChanged(
      (user) => emit({ user }),
      (error) => emit({ error })
    );
    return unsubscribe;
  });
  return authChannel;
}

function* watchForFirebaseAuth() {
  const authChannel = yield call(createAuthChannel);
  try {
    while (true) {
      const { user: userResult } = yield take(authChannel);
      if (userResult) {
        const { claims: claimsResult } = yield call([
          userResult,
          userResult.getIdTokenResult,
        ]);
        const { uid } = userResult;
        const { isAdmin, theme } = claimsResult;
        const user = { uid };
        const claims = { isAdmin, theme };
        yield put(authUserAvailableAction({ claims, user }));
      } else {
        yield put(authUserUnavailableAction());
      }
    }
  } finally {
    authChannel.close();
  }
}

function createClaimChannel(uid: string) {
  const claimChannel = eventChannel((emit) => {
    const document = firebase.firestore().collection('user-claims').doc(uid);
    const unsubscribe = document.onSnapshot(
      (snapshot) => emit({ claims: snapshot.data() }),
      (error) => emit({ error })
    );

    return unsubscribe;
  });
  return claimChannel;
}

function* watchForClaimUpdate() {
  const user = yield select(selectUser);
  const claimChannel = yield call(createClaimChannel, user.uid);
  while (true) {
    const { claims, error } = yield take(claimChannel);

    if (claims) {
      const { isAdmin, theme } = claims;
      yield put(
        authUserNewClaimsAvailableAction({ claims: { isAdmin, theme } })
      );
    } else if (error) {
      claimChannel.close();
    }
  }
}

export default function* formSaga() {
  yield call(console.log, 'Auth sagas loaded.');
  yield fork(watchForFirebaseAuth);
  yield takeLatest(ActionTypes.AUTH_USER_AVAILABLE, watchForClaimUpdate);
}
