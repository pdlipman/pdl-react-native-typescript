import { createSelector } from 'reselect';

import { ApplicationState } from '@state';

const selectAuth = (state: ApplicationState) => state.auth;
const selectClaims = createSelector(selectAuth, (auth) => auth.claims);

export const selectIsAdmin = createSelector(
  selectClaims,
  (claims) => claims.isAdmin
);

export const selectTheme = createSelector(
  selectClaims,
  (claims) => claims.theme
);

export const selectUser = createSelector(selectAuth, (auth) => auth.user);
export const selectUid = createSelector(selectUser, (user) => user.uid);
