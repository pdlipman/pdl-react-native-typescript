import AsyncStorage from '@react-native-community/async-storage';
import { createReducer } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';

import {
  authStateChangedAction,
  authUserAvailableAction,
  authUserNewClaimsAvailableAction,
  authUserUnavailableAction,
} from './actions';
import { ContainerState } from './types';

const initialState: ContainerState = {
  claims: { isAdmin: false, theme: 'standard' },
  status: 'auth initial',
  user: { uid: '' },
};

const persistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  blacklist: ['status'],
};

const authReducer = createReducer(initialState, (builder) => {
  return builder
    .addCase(authStateChangedAction, (state) => ({
      ...state,
      status: 'changed',
    }))
    .addCase(authUserAvailableAction, (state, { payload }) => {
      const { claims, user } = payload;
      const status = 'user available';
      return { ...state, claims, status, user };
    })
    .addCase(authUserNewClaimsAvailableAction, (state, { payload }) => {
      const { claims } = payload;
      const status = 'claims updated';
      return { ...state, claims, status };
    })
    .addCase(authUserUnavailableAction, (state) => {
      const { claims, user } = initialState;
      const status = 'user unavailable';
      return { ...state, claims, status, user };
    });
});

export default persistReducer(persistConfig, authReducer);
