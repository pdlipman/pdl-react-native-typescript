import app from 'firebase/app';
// eslint-disable-next-line import/no-duplicates
import 'firebase/auth';
// eslint-disable-next-line import/no-duplicates
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyCu1VD2669eB0w6rkqfraaEUEl9IpgrZa8',
  appId: '1:918172792181:web:b1acb33d422b669b9c8f3c',
  authDomain: 'simple-blog-1d04e.firebaseapp.com',
  databaseURL: 'https://simple-blog-1d04e.firebaseio.com',
  projectId: 'simple-blog-1d04e',
  storageBucket: 'simple-blog-1d04e.appspot.com',
  messagingSenderId: '918172792181',
  measurementId: 'G-6SE0WSEW9B',
};

const firebase = app.initializeApp(config);

export default firebase;
