import { Error } from '@providers/firebase/types';
import {
  createAction,
  createActionWithPayload,
} from '@utils/redux/createAction';

import ActionTypes from './constants';

export const registerCancelledAction = createAction(
  ActionTypes.REGISTER_CANCELLED
);

export const registerErrorAction = createActionWithPayload<{ error: Error }>(
  ActionTypes.REGISTER_ERROR
);

export const registerInitializeAction = createAction(
  ActionTypes.REGISTER_INITIALIZE
);

export const registerRequestAction = createActionWithPayload<{
  email: string;
  password: string;
}>(ActionTypes.REGISTER_REQUEST);

export const registerSuccessAction = createAction(ActionTypes.REGISTER_SUCCESS);
