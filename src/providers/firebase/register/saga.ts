import { call, cancelled, fork, put, take } from 'redux-saga/effects';

import firebase from '@providers/firebase';

import {
  registerCancelledAction,
  registerErrorAction,
  registerSuccessAction,
} from './actions';
import ActionTypes from './constants';

function* register(email: string, password: string) {
  try {
    yield call(
      [firebase.auth(), firebase.auth().createUserWithEmailAndPassword],
      email,
      password
    );
    yield put(registerSuccessAction());
  } catch (error) {
    const { code, message } = error;
    yield put(registerErrorAction({ error: { code, message } }));
  } finally {
    if (yield cancelled()) {
      yield put(registerCancelledAction());
    }
  }
}

function* registerFlow() {
  while (true) {
    const { payload } = yield take(ActionTypes.REGISTER_REQUEST);
    const { email, password } = payload;

    yield fork(register, email, password);
  }
}

export default function* registerSaga() {
  yield call(console.log, 'Register sagas loaded.');
  yield fork(registerFlow);
}
