import { createSelector } from 'reselect';

import { ApplicationState } from '@state';

const selectRegister = (state: ApplicationState) => state.register;

const selectError = createSelector(
  selectRegister,
  (register) => register.error
);
export const selectErrorCode = createSelector(
  selectError,
  (error) => error?.code
);
export const selectErrorMessage = createSelector(
  selectError,
  (error) => error?.message
);

const selectStatus = createSelector(
  selectRegister,
  (register) => register.status
);
