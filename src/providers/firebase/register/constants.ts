import { ActionType } from 'plop';

enum ActionTypes {
  REGISTER_CANCELLED = 'providers/firebase/register/REGISTER_CANCELLED',
  REGISTER_ERROR = 'providers/firebase/register/REGISTER_ERROR',
  REGISTER_INITIALIZE = 'providers/firebase/register/REGISTER_INITIALIZE',
  REGISTER_REQUEST = 'providers/firebase/register/REGISTER_REQUEST',
  REGISTER_SUCCESS = 'providers/firebase/register/REGISTER_SUCCESS',
}

export default ActionTypes;
