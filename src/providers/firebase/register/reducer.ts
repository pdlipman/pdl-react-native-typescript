import { createReducer } from '@reduxjs/toolkit';

import {
  registerCancelledAction,
  registerErrorAction,
  registerInitializeAction,
  registerRequestAction,
  registerSuccessAction,
} from './actions';
import { ContainerState } from './types';

const initialState: ContainerState = {
  error: { code: '', message: '' },
  status: 'register initial',
};

const registerReducer = createReducer(initialState, (builder) => {
  return builder
    .addCase(registerCancelledAction, (state) => ({
      ...state,
      error: initialState.error,
      status: 'register cancelled',
    }))
    .addCase(registerErrorAction, (state, { payload }) => {
      const { error } = payload;
      return { ...state, error, status: 'register error' };
    })
    .addCase(registerInitializeAction, () => ({
      ...initialState,
    }))
    .addCase(registerRequestAction, (state) => ({
      ...state,
      error: initialState.error,
      status: 'register request',
    }))
    .addCase(registerSuccessAction, (state) => ({
      ...state,
      error: initialState.error,
      status: 'register success',
    }));
});

export default registerReducer;
