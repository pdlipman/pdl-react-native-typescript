import { Error } from '@providers/firebase/types';

export interface RegisterState {
  readonly error?: Error;
  readonly status: string;
}

type ContainerState = RegisterState;
export { ContainerState };
