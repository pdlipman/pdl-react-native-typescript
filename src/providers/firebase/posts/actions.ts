import { Error } from '@providers/firebase/types';
import {
  createAction,
  createActionWithPayload,
} from '@utils/redux/createAction';

import ActionTypes from './constants';
import { Post } from './types';

export const postsErrorAction = createActionWithPayload<{ error: Error }>(
  ActionTypes.POSTS_ERROR
);

export const postsRequestAction = createAction(ActionTypes.POSTS_REQUEST);
export const postsSuccessAction = createActionWithPayload<{ posts: Post[] }>(
  ActionTypes.POSTS_SUCCESS
);
