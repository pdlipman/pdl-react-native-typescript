import { eventChannel } from 'redux-saga';
import { put, take, takeLatest } from 'redux-saga/effects';

import firebase from '@providers/firebase';

import { postsErrorAction, postsSuccessAction } from './actions';
import ActionTypes from './constants';
import { Post } from './types';

function* getPosts() {
  const documents = firebase
    .firestore()
    .collection('posts')
    .orderBy('datetime');

  const postsChannel = eventChannel((emit) => {
    const unsubscribe = documents.onSnapshot(
      (snapshot) => emit({ snapshot }),
      (error) => emit({ error })
    );

    return unsubscribe;
  });

  while (true) {
    const { snapshot, error } = yield take(postsChannel);
    if (snapshot) {
      const posts: Post[] = [];
      snapshot.forEach((doc: any) => {
        const data = doc.data();
        const {
          content,
          coverImage,
          datetime: { seconds },
          slug,
          title,
        } = data;
        posts.push({ content, coverImage, datetime: seconds, slug, title });
      });
      yield put(postsSuccessAction({ posts }));
    } else if (error) {
      const { code, message } = error;
      yield put(postsErrorAction({ error: { code, message } }));
    }
  }
}

export default function* postsSaga() {
  yield takeLatest(ActionTypes.POSTS_REQUEST, getPosts);
}
