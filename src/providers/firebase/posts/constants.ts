enum ActionTypes {
  POSTS_ERROR = 'providers/firebase/posts/POSTS_ERROR',
  POSTS_REQUEST = 'providers/firebase/posts/POSTS_REQUEST',
  POSTS_SUCCESS = 'providers/firebase/posts/POSTS_SUCCESS',
}

export default ActionTypes;
