import { Error } from '@providers/firebase/types';

export interface Post {
  content: string;
  coverImage: string;
  datetime: Date;
  slug: string;
  title: string;
}

export interface PostsState {
  readonly error?: Error;
  readonly posts: Post[];
  readonly status: string;
}

type ContainerState = PostsState;
export { ContainerState };
