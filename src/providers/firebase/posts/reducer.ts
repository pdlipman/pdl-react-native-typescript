import { createReducer } from '@reduxjs/toolkit';

import {
  postsErrorAction,
  postsRequestAction,
  postsSuccessAction,
} from './actions';
import { ContainerState } from './types';

const initialState: ContainerState = {
  error: { code: '', message: '' },
  posts: [],
  status: 'posts initial',
};

const postsReducer = createReducer(initialState, (builder) => {
  return builder
    .addCase(postsErrorAction, (state, { payload }) => {
      const { error } = payload;
      return { ...state, error, status: 'posts error' };
    })
    .addCase(postsRequestAction, (state) => ({
      ...state,
      error: initialState.error,
      status: 'posts request',
    }))
    .addCase(postsSuccessAction, (state, { payload }) => {
      const { posts } = payload;
      return {
        ...state,
        error: initialState.error,
        posts,
        status: 'posts success',
      };
    });
});

export default postsReducer;
