import { createSelector } from 'reselect';

import { ApplicationState } from '@state';

const selectPosts = (state: ApplicationState) => state.posts;

export const selectAllPosts = createSelector(
  selectPosts,
  (posts) => posts.posts
);
