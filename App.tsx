import { Feather } from '@expo/vector-icons';
import { AppLoading } from 'expo';
import { loadAsync } from 'expo-font';
import React, { useState } from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import Main from '@containers/main';
import store, { persistor } from '@store';

import 'react-native-gesture-handler';

const cacheFonts = (fonts: { [x: string]: any }[]) => {
  return fonts.map((font) => loadAsync(font));
};

const App = () => {
  const [isReady, setIsReady] = useState(false);
  const loadAssetsAsync = async () => {
    const fontAssets = cacheFonts([Feather.font]);
    await Promise.all([...fontAssets]);
  };

  if (!isReady) {
    return (
      <AppLoading
        startAsync={loadAssetsAsync}
        onFinish={() => setIsReady(true)}
        onError={console.warn}
      />
    );
  }
  return (
    <StoreProvider store={store}>
      <PersistGate persistor={persistor}>
        <Main />
      </PersistGate>
    </StoreProvider>
  );
};

export default App;
